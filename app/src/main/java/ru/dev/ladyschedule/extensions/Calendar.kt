package ru.dev.ladyschedule.extensions

import ru.dev.ladyschedule.models.Day
import ru.dev.ladyschedule.models.Intim
import java.util.*


fun Calendar.getNowDay(): Day {
    val calendar = Calendar.getInstance()
    val day = calendar.get(Calendar.DAY_OF_MONTH)
    val month = calendar.get(Calendar.MONTH)
    val year = calendar.get(Calendar.YEAR)
    return Day(day, month, year)
}