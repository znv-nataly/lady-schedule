package ru.dev.ladyschedule.models

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import ru.dev.ladyschedule.repositories.db.converters.IntimConverter
import ru.dev.ladyschedule.repositories.db.converters.MensesConverter

@TypeConverters(IntimConverter::class, MensesConverter::class)
@Entity
data class Day(
    var day: Int,
    var month: Int,
    var year: Int,
    var number: Int = 0,
    var menses: Menses = Menses.ABSENT,
    var intim: Intim = Intim.ABSENT,
    var note: String = "",


    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    var id: String = "$day" + "_" + "$month" + "_" + "$year"
)

enum class Intim(val value: Int) {
    ABSENT(0),
    CONDOM_WITHOUT(1),
    CONDOM_WITH(2)
}

enum class Menses(val value: Int) {
    ABSENT(0),
    END(1),
    IS(2)
}