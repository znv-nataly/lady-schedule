package ru.dev.ladyschedule.repositories

import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import ru.dev.ladyschedule.App
import ru.dev.ladyschedule.extensions.getNowDay
import ru.dev.ladyschedule.models.Day
import ru.dev.ladyschedule.models.Intim
import java.util.*


object PreferencesRepository {

    private const val DAY = "DAY"
    private const val MONTH = "MONTH"
    private const val YEAR = "YEAR"

    fun getDay(): Day {
        val nowDay = Calendar.getInstance().getNowDay()
        return Day(
                prefs.getInt(DAY, nowDay.day),
                prefs.getInt(MONTH, nowDay.month),
                prefs.getInt(YEAR, nowDay.year)
        )
    }

    fun saveDay(dayData: Day) {
        with(dayData) {
            putValue(DAY to day)
            putValue(MONTH to month)
            putValue(YEAR to year)
        }
    }

    private val prefs: SharedPreferences by lazy {
        val context = App.appContext()
        PreferenceManager.getDefaultSharedPreferences(context)
    }

    private fun putValue(pair: Pair<String, Any>) = with(prefs.edit()) {
        val key = pair.first
        when(val value = pair.second) {
            is String -> putString(key, value)
            is Int -> putInt(key, value)
            is Float -> putFloat(key, value)
            is Long -> putLong(key, value)
            is Boolean -> putBoolean(key, value)
            else -> error("Only primitives types can be stored in Shared Preferences")
        }
        apply()
    }
}