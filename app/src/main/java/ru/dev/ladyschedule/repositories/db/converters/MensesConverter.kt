package ru.dev.ladyschedule.repositories.db.converters

import androidx.room.TypeConverter
import ru.dev.ladyschedule.models.Menses

class MensesConverter {

    companion object {
        @JvmStatic
        @TypeConverter
        fun intToMenses(value: Int): Menses = Menses.values()[value]

        @JvmStatic
        @TypeConverter
        fun mensesToInt(value: Menses): Int = value.value
    }
}