package ru.dev.ladyschedule.repositories.db.converters

import androidx.room.TypeConverter
import ru.dev.ladyschedule.models.Intim


class IntimConverter {

    companion object {
        @JvmStatic
        @TypeConverter
        fun intToIntim(value: Int): Intim = Intim.values()[value]

        @JvmStatic
        @TypeConverter
        fun intimToInt(value: Intim): Int = value.value
    }
}