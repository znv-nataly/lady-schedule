package ru.dev.ladyschedule.repositories.db.dao

import androidx.room.*
import ru.dev.ladyschedule.models.Day

@Dao
interface DayDao {

    @Query("SELECT * FROM day WHERE month = :month AND year = :year")
    fun getDaysMonth(month: Int, year: Int): List<Day>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDay(day: Day)

    @Query("DELETE FROM day WHERE number = 0 AND menses = 0 AND intim = 0 AND note = \"\"")
    fun deleteEmptyDays()

}