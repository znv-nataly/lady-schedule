package ru.dev.ladyschedule.repositories.db

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.dev.ladyschedule.models.Day
import ru.dev.ladyschedule.repositories.db.dao.DayDao

@Database(entities = [Day::class], version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun dayDao(): DayDao
}