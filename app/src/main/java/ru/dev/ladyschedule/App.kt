package ru.dev.ladyschedule

import android.app.Application
import android.content.Context
import androidx.room.Room
import ru.dev.ladyschedule.repositories.db.AppDatabase


class App: Application() {

    companion object {
        private var instance: App? = null
        private var db: AppDatabase? = null

        fun appContext(): Context = instance!!.applicationContext

        fun appDb(): AppDatabase = db!!
    }

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()

        db = Room.databaseBuilder(
            this,
            AppDatabase::class.java,
            "lady_schedule_db"
        ).build()
    }
}