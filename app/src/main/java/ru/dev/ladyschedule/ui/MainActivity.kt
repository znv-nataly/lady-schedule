package ru.dev.ladyschedule.ui

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import ru.dev.ladyschedule.R
import ru.dev.ladyschedule.databinding.ActivityMainBinding
import ru.dev.ladyschedule.databinding.DialogAboutBinding
import ru.dev.ladyschedule.databinding.DialogDayOptionsBinding
import ru.dev.ladyschedule.extensions.toEditable
import ru.dev.ladyschedule.models.Day
import ru.dev.ladyschedule.models.Intim
import ru.dev.ladyschedule.models.Menses
import ru.dev.ladyschedule.ui.adapters.DayCalendarAdapter
import ru.dev.ladyschedule.viewmodels.MainViewModel
import java.util.*

//@Suppress("UNREACHABLE_CODE")
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var dayAdapter: DayCalendarAdapter

    companion object {
        private const val MENU_ABOUT = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initToolbar()
        initViews()
        initViewModel()

        updateToolbarTitle(viewModel.getSelectedDay())
        viewModel.deleteEmptyDays()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.deleteEmptyDays()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu?.add(0, MENU_ABOUT, 0, getString(R.string.menu_about))
        return super.onCreateOptionsMenu(menu)
    }

    @SuppressLint("SetTextI18n")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == MENU_ABOUT) {
            val dialogBuilder = AlertDialog.Builder(this)
            val dialogBinding = DialogAboutBinding.inflate(layoutInflater)
            val version = packageManager.getPackageInfo(packageName, 0).versionName
            dialogBinding.tvVersion.text = "${resources.getString(R.string.version_prefix)} $version"
            dialogBuilder.setView(dialogBinding.root)
            dialogBuilder.create().show()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initToolbar() {
        setSupportActionBar(binding.toolbar)
    }

    private fun initViews() {
        dayAdapter = DayCalendarAdapter(
                this,
                { day: Day -> dayClick(day) },
                { day: Day -> dayLongClick(day) }
        )
        binding.gvCalendar.adapter = dayAdapter

        binding.ivPrevMonth.setOnClickListener { viewModel.monthPrev() }
        binding.ivNextMonth.setOnClickListener { viewModel.monthNext() }
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this@MainActivity).get(MainViewModel::class.java)
        viewModel.getViewMonthYearData().observe(this, { updateScheduleTitle(it) })
        viewModel.getDaysMonthData().observe(this, { updateSchedule(it) })
    }

    private fun updateScheduleTitle(monthYear: Pair<Int, Int>) {
        val months = resources.getStringArray(R.array.months)
        val title = months[monthYear.first] + " " + monthYear.second
        binding.tvMonth.text = title
    }

    private fun updateSchedule(daysMonth: List<Any?>) {
        dayAdapter.updateDays(daysMonth, viewModel.getSelectedDay())
    }

    private fun updateToolbarTitle(day: Day) {
        val monthsLowerCase = resources.getStringArray(R.array.months_lower_case).toList()
        val daysOfWeek = resources.getStringArray(R.array.days_of_week).toList()
        val title = viewModel.getDayTitle(day, monthsLowerCase, daysOfWeek)
        supportActionBar!!.title = title
    }

    private fun dayClick(day: Day) {
        viewModel.setSelectedDay(day)
        dayAdapter.updateDays(viewModel.getDaysMonth(), viewModel.getSelectedDay())
        updateToolbarTitle(day)
    }

    private fun dayLongClick(day: Day): Boolean {
        if(viewModel.getSelectedDay() != day) {
            dayClick(day)
        }

        if(isFutureDay(day)) { // for future days options doesn't show
            return true
        }

        val dialogBinding = DialogDayOptionsBinding.inflate(layoutInflater)
        with(dialogBinding) {
            cbBegin.isChecked = day.number == 1
            cbEnd.isChecked = day.menses == Menses.END
            etNote.text = day.note.toEditable()
            cbBegin.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    dialogBinding.cbEnd.isChecked = false
                }
            }
            cbEnd.setOnCheckedChangeListener{ _, isChecked ->
                if (isChecked) {
                    dialogBinding.cbBegin.isChecked = false
                }
            }
            cbEnd.isEnabled = day.number > 0
        }
        setIntimValue(dialogBinding, day.intim)

        val dialogBuilder = AlertDialog.Builder(this)
        with(dialogBuilder) {
            setView(dialogBinding.root)
            setNegativeButton(R.string.btn_cancel) { dialog, _ -> dialog.dismiss() }
            setPositiveButton(R.string.btn_save) {
                dialog, _ -> saveDayOptions(day, dialogBinding, dialog)
            }
        }
        dialogBuilder.create().show()
        return false
    }

    private fun saveDayOptions(oldDay: Day, dialogBinding: DialogDayOptionsBinding, dialog: DialogInterface) {
        val newDay = Day(oldDay.day, oldDay.month, oldDay.year)
        with(newDay) {
            number = if (dialogBinding.cbBegin.isChecked) 1 else { if (oldDay.number == 1) 0 else oldDay.number }
            menses = getMensesValue(dialogBinding, oldDay)
            intim = getIntimValue(dialogBinding)
            note = dialogBinding.etNote.text.toString()
        }
        viewModel.addDay(newDay)
        dialog.dismiss()
    }

    private fun getIntimValue(dialogBinding: DialogDayOptionsBinding): Intim {
        return when (dialogBinding.rgIntim.checkedRadioButtonId) {
            dialogBinding.rbIntimAbsent.id -> Intim.ABSENT
            dialogBinding.rbIntimCondomWith.id -> Intim.CONDOM_WITH
            dialogBinding.rbIntimCondomWithout.id -> Intim.CONDOM_WITHOUT
            else -> Intim.ABSENT
        }
    }

    private fun getMensesValue(dialogBinding: DialogDayOptionsBinding, day: Day): Menses {
        return when {
            dialogBinding.cbBegin.isChecked -> Menses.IS
            dialogBinding.cbEnd.isChecked -> Menses.END
            day.number == 1 -> Menses.ABSENT
            day.menses == Menses.END -> Menses.ABSENT
            else -> day.menses
        }
    }

    private fun setIntimValue(dialogBinding: DialogDayOptionsBinding, intim: Intim) {
        when (intim) {
            Intim.ABSENT -> dialogBinding.rgIntim.check(dialogBinding.rbIntimAbsent.id)
            Intim.CONDOM_WITH -> dialogBinding.rgIntim.check(dialogBinding.rbIntimCondomWith.id)
            Intim.CONDOM_WITHOUT -> dialogBinding.rgIntim.check(dialogBinding.rbIntimCondomWithout.id)
        }
    }

    private fun isFutureDay(day: Day): Boolean {
        val calendar = Calendar.getInstance()
        calendar.set(day.year, day.month, day.day)

        return calendar.timeInMillis > Calendar.getInstance().timeInMillis
    }
}