package ru.dev.ladyschedule.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import ru.dev.ladyschedule.R
import ru.dev.ladyschedule.extensions.getNowDay
import ru.dev.ladyschedule.models.Day
import ru.dev.ladyschedule.models.Intim
import ru.dev.ladyschedule.models.Menses
import ru.dev.ladyschedule.ui.custom.GridViewSquareCells
import java.util.*


class DayCalendarAdapter(
    private val context: Context,
    private var clickListener: (Day) -> Unit,
    private var longClickListener: (Day) -> Boolean
): BaseAdapter() {

    private var items: List<Any?> = listOf()
    private var selectedDay: Day = Calendar.getInstance().getNowDay()
    private var namesDaysOfWeek: Array<out String> = context.resources.getStringArray(R.array.days_of_week)

    override fun getCount(): Int = items.size

    override fun getItem(position: Int): Any? = items[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val gridView = parent as GridViewSquareCells
        val size = gridView.columnWidth

        var itemView: View? = convertView
        if (itemView == null) {
            itemView = LayoutInflater.from(context).inflate(R.layout.day_item, parent, false)
            itemView!!.layoutParams = AbsListView.LayoutParams(size, size)
        }
        bindDayItem(itemView, position)
        return itemView
    }

    private fun bindDayItem(itemView: View, position: Int) {
        val tvDay = itemView.findViewById<TextView>(R.id.tv_day)
        val tvNumDay = itemView.findViewById<TextView>(R.id.tv_num_day)
        val ivIntimCondomWith = itemView.findViewById<ImageView>(R.id.iv_intim_condom)
        val ivIntim = itemView.findViewById<ImageView>(R.id.iv_intim)
        val ivNote = itemView.findViewById<ImageView>(R.id.iv_note)

        when(val day = items[position]) {
            null -> {
                tvDay.text = ""
                tvNumDay.text = ""
                ivIntimCondomWith.visibility = View.GONE
                ivIntim.visibility = View.GONE
                ivNote.visibility = View.GONE
                itemView.setBackgroundColor(context.getColor(R.color.color_bg_grid))
            }
            is Int -> {
                tvDay.text = namesDaysOfWeek[day]
                tvNumDay.text = ""
                ivIntimCondomWith.visibility = View.GONE
                ivIntim.visibility = View.GONE
                ivNote.visibility = View.GONE
                itemView.setBackgroundColor(context.getColor(R.color.color_bg_grid))
            }
            is Day -> {
                tvDay.text = day.day.toString()
                tvNumDay.text = if (day.number > 0) day.number.toString() else ""
                val nowDay = Calendar.getInstance().getNowDay()
                when (day.id) {
                    nowDay.id -> {
                        if (day.menses == Menses.ABSENT) {
                            itemView.setBackgroundResource(R.drawable.bg_day_now)
                        } else {
                            itemView.setBackgroundResource(R.drawable.bg_day_now_menses)
                        }
                    }
                    selectedDay.id -> {
                        if (day.menses == Menses.ABSENT) {
                            itemView.setBackgroundResource(R.drawable.bg_day_select)
                        } else {
                            itemView.setBackgroundResource(R.drawable.bg_day_select_menses)
                        }
                    }
                    else -> {
                        if (day.menses == Menses.ABSENT) {
                            itemView.setBackgroundResource(R.color.color_bg_cells)
                        } else {
                            itemView.setBackgroundResource(R.color.color_bg_cell_menses)
                        }
                    }
                }
                when(day.intim) {
                    Intim.ABSENT -> {
                        ivIntim.visibility = View.GONE
                        ivIntimCondomWith.visibility = View.GONE
                    }
                    Intim.CONDOM_WITH -> {
                        ivIntim.visibility = View.GONE
                        ivIntimCondomWith.visibility = View.VISIBLE
                    }
                    Intim.CONDOM_WITHOUT -> {
                        ivIntim.visibility = View.VISIBLE
                        ivIntimCondomWith.visibility = View.GONE
                    }
                }

                ivNote.visibility = if(day.note.isNotEmpty()) View.VISIBLE else View.GONE

                itemView.setOnClickListener { clickListener.invoke(day) }
                itemView.setOnLongClickListener { longClickListener.invoke(day) }
            }
        }
    }

    fun updateDays(days: List<Any?>, selectedDay: Day) {
        this.items = days
        this.selectedDay = selectedDay
        notifyDataSetChanged()
    }
}