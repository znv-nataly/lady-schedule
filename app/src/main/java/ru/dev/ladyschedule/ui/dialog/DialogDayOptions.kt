package ru.dev.ladyschedule.ui.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import ru.dev.ladyschedule.R
import ru.dev.ladyschedule.databinding.DialogDayOptionsBinding
import ru.dev.ladyschedule.models.Day


class DialogDayOptions(
        private val context: Context,
        private val day: Day
): DialogFragment() {

    private lateinit var binding: DialogDayOptionsBinding

    override fun getContext(): Context {
        return context
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding = DialogDayOptionsBinding.inflate(layoutInflater)

        val builder = AlertDialog.Builder(context)
        builder.setView(binding.root)
        builder.setNegativeButton(R.string.btn_cancel) { d, _ -> d.dismiss() }
        builder.setPositiveButton(R.string.btn_save) { d, _ -> d.dismiss() }

        return builder.create()
    }
}