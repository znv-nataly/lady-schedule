package ru.dev.ladyschedule.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.dev.ladyschedule.R
import ru.dev.ladyschedule.models.Day


class DayAdapter(
    private val listener: (Day) -> Unit
): RecyclerView.Adapter<DayAdapter.DayViewHolder>() {

    var items: MutableList<Day?> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return DayViewHolder(inflater.inflate(R.layout.day_item, parent, false))
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DayViewHolder, position: Int) {
//        holder.bind(items[position], position, listener)
        val item = items[position]
        if (item != null) {
            holder.tvDay?.text = item.day.toString() + " (" + position + ")"
            holder.itemView.setOnClickListener { listener.invoke(item) }
        } else {
            holder.tvDay?.text = "***"
        }
    }

    override fun getItemCount(): Int = items.size

    fun updateDays(days: List<Day?>) {
        items.clear()
        items.addAll(days)
        notifyDataSetChanged()
    }

    inner class DayViewHolder(convertView: View): RecyclerView.ViewHolder(convertView) {

        var tvDay: TextView? = null

        init {
            tvDay = convertView.findViewById<TextView>(R.id.tv_day)
        }

//        @SuppressLint("SetTextI18n")
//        fun bind(item: Day?, position: Int, listener: (Day) -> Unit) {
//            if (item != null) {
//                tvDay?.text = item.day.toString() + " (" + position + ")"
//                itemView.setOnClickListener { listener.invoke(item) }
//            } else {
//                tvDay?.text = ""
//            }
//        }
    }
}