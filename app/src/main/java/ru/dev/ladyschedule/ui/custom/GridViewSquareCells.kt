package ru.dev.ladyschedule.ui.custom

import android.app.ActionBar
import android.content.Context
import android.util.AttributeSet
import android.widget.GridView

// @JvmOverloads при компиляции будет создано 3 конструктора, для каждого из возможных аргументов
class GridViewSquareCells @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
): GridView(context, attrs, defStyleAttr) {

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var heightSpec = heightMeasureSpec
        // set height GidView by content
        if (layoutParams.height == ActionBar.LayoutParams.WRAP_CONTENT) {
            heightSpec = MeasureSpec.makeMeasureSpec((Int.MAX_VALUE shr 2), MeasureSpec.AT_MOST)
        }
        super.onMeasure(widthMeasureSpec, heightSpec)
    }
}