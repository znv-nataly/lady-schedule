package ru.dev.ladyschedule.viewmodels

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.dev.ladyschedule.App
import ru.dev.ladyschedule.extensions.getNowDay
import ru.dev.ladyschedule.models.Day
import ru.dev.ladyschedule.models.Menses
import ru.dev.ladyschedule.repositories.PreferencesRepository
import ru.dev.ladyschedule.repositories.db.AppDatabase
import ru.dev.ladyschedule.repositories.db.dao.DayDao
import java.util.*


class MainViewModel: ViewModel() {

    companion object {
        private const val COUNT_DAYS_IN_WEEK = 7
    }
    private val tag = MainViewModel::class.java.simpleName.toString()
    private val repository: PreferencesRepository = PreferencesRepository
    private val appDb: AppDatabase = App.appDb()
    private val dayDao: DayDao = appDb.dayDao()
    private var selectedDay: Day = repository.getDay()

    private var viewMonthYearData = MutableLiveData<Pair<Int, Int>>()

    private var daysMonthData = MutableLiveData<List<Any?>>()


    init {
        val nowDay: Day = Calendar.getInstance().getNowDay()
        viewMonthYearData.value = nowDay.month to nowDay.year
        daysMonthData.value = listOf()
        getDaysMonth(nowDay.month, nowDay.year)
    }

    fun getViewMonthYearData(): LiveData<Pair<Int, Int>> = viewMonthYearData

    fun getDaysMonthData(): LiveData<List<Any?>> = daysMonthData

    fun getDaysMonth(): List<Any?> = daysMonthData.value!!

    fun getSelectedDay(): Day = selectedDay

    fun setSelectedDay(newSelectedDay: Day) {
        selectedDay= newSelectedDay
    }

    fun monthPrev() {
        val p = viewMonthYearData.value!!
        var viewMonth = p.first
        var viewYear = p.second
        if (viewMonth == 0) {
            viewMonth = 11
            viewYear--
        } else {
            viewMonth--
        }
        viewMonthYearData.value = viewMonth to viewYear

        getDaysMonth(viewMonth, viewYear)
    }

    fun monthNext() {
        val p = viewMonthYearData.value!!
        var viewMonth = p.first
        var viewYear = p.second
        if (viewMonth == 11) {
            viewMonth = 0
            viewYear++
        } else {
            viewMonth++
        }
        viewMonthYearData.value = viewMonth to viewYear

        getDaysMonth(viewMonth, viewYear)
    }

    @SuppressLint("CheckResult")
    fun deleteEmptyDays() {
        Observable.fromCallable { dayDao.deleteEmptyDays() }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d(tag, "Delete empty days")
            }
    }

    /**
     * Insert day data into db and return result in main thread
     */
    @SuppressLint("CheckResult")
    fun addDay(day: Day) {
        val checkDay = checkDayBeforeSave(day)
        Observable.fromCallable { dayDao.insertDay(checkDay) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d(tag, "Insert day id ${checkDay.id}")

                val days = daysMonthData.value!! as MutableList<Any?>
                val resultSearch = days.filter { it is Day && it.day == checkDay.day && it.month == checkDay.month && it.year == checkDay.year }
                if (resultSearch.isNotEmpty()) {
                    val resultDay = resultSearch.first() as Day
                    val i = days.indexOf(resultDay)
                    days.remove(resultDay)
                    days.add(i, checkDay)
                }

                val countDaysInMonth = getCountDaysInMonth()
                val daysPrev = days.subList(0, days.size - countDaysInMonth)
                var daysMonth = days.subList(days.size - countDaysInMonth, days.size).map { it as Day }
                daysMonth = getDaysMonthInfo(daysMonth)
                daysPrev.addAll(daysPrev.size, daysMonth)

                daysMonthData.value = daysPrev
            }
    }

    /**
     * Insert day data into db and don'r return result in main thread
     */
    @SuppressLint("CheckResult")
    fun insertDay(day: Day) {
        val checkDay = checkDayBeforeSave(day)
        Observable.fromCallable { dayDao.insertDay(checkDay) }
            .subscribeOn(Schedulers.io())
            .subscribe {
                Log.d(tag, "Insert day id ${checkDay.id}")
            }
    }

    fun getDayTitle(day: Day, months: List<String>, daysOfWeek: List<String>): String {
        val calendar = Calendar.getInstance()
        calendar.set(day.year, day.month, day.day)

        // number day of week in Calendar. Numeration begins from Sunday: Su - 1, Mo - 2, ...
        val dayOfWeekCalendar = calendar.get(Calendar.DAY_OF_WEEK)

        // number day of week in Calendar. Numeration begins from Monday: Mo - 1, Tu - 2, ...
        val dayOfWeek: Int = if (dayOfWeekCalendar - 2 >= 0)
                                dayOfWeekCalendar - 2
                             else COUNT_DAYS_IN_WEEK + dayOfWeekCalendar - 2
        return (daysOfWeek[dayOfWeek] + ", "
                + day.day + " "
                + months[day.month] + " " + day.year)
    }

    private fun checkDayBeforeSave(day: Day): Day {
        if (day.number == 1) {
            day.number = 1
            day.menses = Menses.IS
        }
        day.note = day.note.trim()
        return day
    }

    private fun getCountDaysInMonth(): Int {
        val monthYear = viewMonthYearData.value!!
        val calendar = GregorianCalendar(monthYear.second, monthYear.first, 1)
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
    }

    @SuppressLint("CheckResult")
    private fun getDaysMonth(month: Int, year: Int) {
        Observable.fromCallable {
                dayDao.getDaysMonth(month, year)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                {
                    days -> daysMonthData.value = getAllDaysMonth(days)
                },
                {
                    daysMonthData.value = getAllDaysMonth(listOf())
                })
    }

    private fun getAllDaysMonth(daysInfo: List<Day>): List<Any?> {
        val days: MutableList<Any?> = mutableListOf()

        for (i in (0 until COUNT_DAYS_IN_WEEK)) { // i < firstDayOfWeek
            days.add(i)
        }

        val monthYear = viewMonthYearData.value!!

        // set first day of selected month
        val calendar = GregorianCalendar(monthYear.second, monthYear.first, 1)

        // day of week for first day of month
        var firstDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)

        // number day of week in Calendar. Numeration begins from Monday: Mo - 1, Tu - 1, ...
        firstDayOfWeek = if(firstDayOfWeek - 2 >= 0) firstDayOfWeek - 1 else COUNT_DAYS_IN_WEEK + firstDayOfWeek - 1

        // add empty cells which means last days from prev month
        for(i in (1 until firstDayOfWeek)) {
            days.add(null)
        }
        days.addAll(days.size, getDaysMonthInfo(daysInfo))
        return days
    }

    private fun getDaysMonthInfo(daysInfo: List<Day>): List<Day> {
        val days: MutableList<Day> = mutableListOf()
        val monthYear = viewMonthYearData.value!!
        var number = 0 // number of day in menstruation cycle
        var menses = Menses.ABSENT
        val countDaysInMonth = getCountDaysInMonth()
        for(i in (1..countDaysInMonth)) {
            val dayInfo = daysInfo.filter { it.day == i }
            val day = if (dayInfo.isEmpty()) Day(i, monthYear.first, monthYear.second) else dayInfo.first()

            if (day.number == 1) {
                number = 1
                menses = Menses.IS
            } else if (i == 1 && day.number > 0) {
                number = day.number
                menses = if (day.menses != Menses.ABSENT || day.number <= 10) Menses.IS else menses
            } else if (number > 0) {
                number++
            }

            // for continue numeration in the next month
            if (i == countDaysInMonth) {
                val nextMonth = if (day.month < 11) day.month + 1 else 0
                val year = if (nextMonth == 0) day.year + 1 else day.year

                val firstDayNextMonth = Day(1, nextMonth, year)
                firstDayNextMonth.number = if (number > 0) number + 1 else 0
                firstDayNextMonth.menses = if (day.menses == Menses.IS) Menses.IS else Menses.ABSENT
                if (firstDayNextMonth.number > 0 || firstDayNextMonth.menses == Menses.IS) {
                    insertDay(firstDayNextMonth)
                }
            }
            day.number = if (number <= 60) number else 0
            if (day.menses == Menses.END) {
                menses = Menses.ABSENT
            } else if (number > 10 && menses == Menses.IS) {
                day.menses = Menses.ABSENT
                menses = Menses.ABSENT
            } else {
                day.menses = menses
            }
            days.add(day)
        }
        return days
    }


}